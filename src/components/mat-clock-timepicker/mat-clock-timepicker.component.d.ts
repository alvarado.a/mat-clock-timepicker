import { EventEmitter, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { ITime } from "../mat-clock/mat-clock.component";
export declare class MatTimePickerComponent implements OnInit {
    private dialog;
    color: string;
    userTime: ITime;
    userTimeChange: EventEmitter<ITime>;
    constructor(dialog: MatDialog);
    ngOnInit(): void;
    readonly time: string;
    showPicker($event: any): boolean;
    private emituserTimeChange;
}
