/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import { TimeDialogComponent } from '../mat-time-dialog/mat-time-dialog.component';
var MatTimePickerComponent = /** @class */ (function () {
    function MatTimePickerComponent(dialog) {
        this.dialog = dialog;
        this.userTimeChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    MatTimePickerComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        // this should be removed
        // if (!this.userTime) {
        //   this.userTime = {
        //     hour: 10,
        //     minute: 25,
        //     meriden: "PM",
        //     format: 24
        //   };
        // }
    };
    Object.defineProperty(MatTimePickerComponent.prototype, "time", {
        get: /**
         * @return {?}
         */
        function () {
            if (!this.userTime) {
                return "";
            }
            /** @type {?} */
            var meriden = "" + this.userTime.meriden;
            if (this.userTime.format === 24) {
                meriden = "";
            }
            /** @type {?} */
            var hour = "" + this.userTime.hour;
            if (this.userTime.hour === 24) {
                hour = "00";
            }
            if (this.userTime.minute === 0) {
                return hour + ":00 " + meriden;
            }
            else if (this.userTime.minute < 10) {
                /** @type {?} */
                var tt = "0" + String(this.userTime.minute);
                return hour + ":" + tt + " " + meriden;
            }
            else {
                return hour + ":" + this.userTime.minute + " " + meriden;
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} $event
     * @return {?}
     */
    MatTimePickerComponent.prototype.showPicker = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        var _this = this;
        /** @type {?} */
        var dialogRef = this.dialog.open(TimeDialogComponent, {
            data: {
                time: {
                    hour: this.userTime.hour,
                    minute: this.userTime.minute,
                    meriden: this.userTime.meriden,
                    format: this.userTime.format
                },
                color: this.color
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            // result will be update userTime object or -1 or undefined (closed dialog w/o clicking cancel)
            if (result === undefined) {
                return;
            }
            else if (result !== -1) {
                _this.userTime = result;
                _this.emituserTimeChange();
            }
        });
        return false;
    };
    /**
     * @return {?}
     */
    MatTimePickerComponent.prototype.emituserTimeChange = /**
     * @return {?}
     */
    function () {
        this.userTimeChange.emit(this.userTime);
    };
    MatTimePickerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'mat-clock-timepicker',
                    template: "<mat-form-field class=\"timeContainer\">\n  <input\n    matInput\n    class=\"timeInput\"\n    placeholder=\"Selec time\"\n    name=\"time_Control\"\n    [value]=\"time\"\n    (click)=\"showPicker($event)\"\n    readonly\n  />\n\n  <button\n    matSuffix\n    mat-button\n    (click)=\"showPicker($event)\"\n    class=\"time-picker-button\"\n  >\n    <mat-icon>access_time</mat-icon>\n  </button>\n</mat-form-field>\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: [".time-picker-button {\n  padding: 0 !important;\n  margin: 0 !important;\n  min-width: 0 !important;\n  width: 40px;\n  height: 40px;\n  flex-shrink: 0;\n  line-height: 40px !important;\n  border-radius: 50% !important;\n  width: 100%; }\n"]
                }] }
    ];
    /** @nocollapse */
    MatTimePickerComponent.ctorParameters = function () { return [
        { type: MatDialog }
    ]; };
    MatTimePickerComponent.propDecorators = {
        color: [{ type: Input }],
        userTime: [{ type: Input }],
        userTimeChange: [{ type: Output }]
    };
    return MatTimePickerComponent;
}());
export { MatTimePickerComponent };
if (false) {
    /** @type {?} */
    MatTimePickerComponent.prototype.color;
    /** @type {?} */
    MatTimePickerComponent.prototype.userTime;
    /** @type {?} */
    MatTimePickerComponent.prototype.userTimeChange;
    /** @type {?} */
    MatTimePickerComponent.prototype.dialog;
}
//# sourceMappingURL=mat-clock-timepicker.component.js.map
